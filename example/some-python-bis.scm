(define python "python")

(specifications->manifest
 (append
  (list python)
  (map (lambda (pkg)
         (string-append python "-" pkg))
       (list
        "matplotlib"
        "numpy"
        "scipy"))))
