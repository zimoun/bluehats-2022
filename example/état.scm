(list (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (branch "master")
        (commit
          "00ff6f7c399670a76efffb91276dea2633cc130c")))
