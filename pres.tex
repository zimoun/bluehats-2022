\documentclass[aspectratio=169,ignorenonframetext,presentation]{beamer}

%
% Header
%

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[french]{babel}
\usepackage{fancyvrb}

\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing, positioning, arrows.meta}

\newcommand{\tikzmark}[2]{%
  \tikz[remember picture,baseline=(#1.base)]%
  {\node[inner sep=0pt] (#1) {#2 \quad};}}

\newcommand{\tikzbrace}[3]{%
  \begin{tikzpicture}[remember picture,overlay]
    \draw[decorate,decoration={brace}] (#1.north east) -- node[right]
    { % XXXX: hfill
      #3}
    (#1.north east |- #2.south east);
  \end{tikzpicture}}


\usetheme{Madrid}

\makeatletter
\usecolortheme{whale}
\usecolortheme{orchid}
\setbeamerfont{block title}{size={}}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{itemize items}[triangle]
\makeatletter
\setbeamertemplate{footline}
{
  \leavevmode%
  \hbox{%
    \begin{beamercolorbox}[wd=.15\paperwidth,ht=2.25ex,dp=1ex,left]{author in head/foot}%
      \usebeamerfont{author in head/foot}\quad S. Tournier
    \end{beamercolorbox}%
    \begin{beamercolorbox}[wd=.7\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
      \usebeamerfont{title in head/foot}\insertshorttitle
    \end{beamercolorbox}%
    \begin{beamercolorbox}[wd=.15\paperwidth,ht=2.25ex,dp=1ex,right]{date in head/foot}%
      \insertframenumber{} / \inserttotalframenumber\hspace*{2ex}
    \end{beamercolorbox}}%
  \vskip0pt%
}
\makeatother

\title[Reproductibilité \emph{computationnelle}, solution avec Guix ?]%
{GNU Guix, vers la reproductibilité computationnelle ?}
\author{Simon Tournier}
\date{Journée BlueHats, 8 novembre 2022}
\institute{Inserm US53 - UAR CNRS 2030 \\ \texttt{simon.tournier@inserm.fr}}
\hypersetup{
  pdfauthor={Simon Tournier},
  pdftitle={GNU Guix, vers la reproductibilité computationnelle ?},
  pdfkeywords={GNU Guix, reproductibilité, déploiement logiciels, gestionnaire de paquets, machine virtuelle},
  pdfsubject={GNU Guix},
  pdfcreator={with love},
  pdflang={French}}


\newcommand{\violet}{\textcolor{violet}}
\newcommand{\blue}{\textcolor{blue}}
\newcommand{\red}{\textcolor{red}}
\newcommand{\magenta}{\textcolor{magenta}}
\newcommand{\cyan}{\textcolor{cyan}}
\newcommand{\green}{\textcolor{green}}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\newcommand{\mauve}{\textcolor{mauve}}

\input{listings-scheme}

\newcommand{\someguile}{\textcolor{someguile}}
\newcommand{\somestring}{\textcolor{somestring}}
\newcommand{\somevariable}{\textcolor{somevariable}}

\newcommand{\thisslide}[1]{%
  \begin{figure}[!htb]
    \begin{center}
      \includeslide[width=\textwidth]{#1}
    \end{center}
  \end{figure}
}

\newcommand{\smiley}[1]{${\textsf{#1~}}^{\textrm{:-)}}$}

\newcommand{\hrefsf}[2]{\href{#1}{\textsf{#2}}}


%
% Document
%
\begin{document}

\begin{frame}[plain, fragile, noframenumbering]{}
  \vspace{5mm}
  \titlepage

  \vfill{}
  \vspace{-12mm}
  \begin{center}
    \includegraphics[width=0.2\paperwidth]{static/Guix-white}

    \vspace{-10mm}
    \href{https://hpc.guix.info}{\texttt{https://hpc.guix.info}}
  \end{center}
  \vfill{}
  \normalsize
  \begin{minipage}{0.2\paperwidth}
    \vspace{-7mm}
    % \includegraphics[width=0.2\paperwidth]{static/LOGO-JRES-2022}
  \end{minipage}
  \begin{flushright}
    \begin{minipage}{0.2\paperwidth}
      \vspace{-20mm}
      \includegraphics[width=0.2\paperwidth]{static/u-paris}
    \end{minipage}
  \end{flushright}
\end{frame}



\begin{frame}<presentation>[label=why, fragile, plain, noframenumbering]{Pourquoi j'en suis venu à GNU Guix}
  \begin{alertblock}{\(\approx\) 2010 \textbf{Thésard}}
    Développement d'1-2 outils utilisant un gestionnaire de paquets
    \uline{classique}

    \hfill (Simulation numérique \texttt{C} et \texttt{Fortran} avec Debian / Ubuntu /
    \texttt{apt})
  \end{alertblock}


  \begin{exampleblock}{\(\approx\) 2014 \textbf{Post-doc}}
    Développement de 2-3 outils utilisant un gestionnaire de paquets sans
    droit administrateur

    \hfill (Simulation numérique \texttt{Python} et \texttt{C++} avec \texttt{conda})
  \end{exampleblock}

  \begin{block}{2016 \textbf{Ingénieur. de Recherche}}
    \begin{itemize}
    \item Administration d'un \emph{cluster} (\texttt{modulefiles})
    \item Utilisation de 10+ outils pour un même projet
    \end{itemize}

    \hfill (Analyse «~bioinformatique~»)
  \end{block}
  \begin{center}
    \textbf{Question : \alert{pourquoi cela fonctionne-t-il pour Alice et pas pour Bob ? Et vice-versa.}}
  \end{center}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\begin{frame}[fragile]{Le monde est \emph{open}, n'est-ce pas ?}

  \begin{itemize}
  \item \emph{open} journal
  \item \emph{open} data
  \item \emph{open} source
  \item \emph{open} science
  \item \emph{open} etc.
  \end{itemize}

  \begin{exampleblock}{}
    \begin{center}
      Quel est le problème de reproductibilité dans un contexte scientifique ?

      (même si tout devient \emph{open})
    \end{center}
  \end{exampleblock}

  \begin{center}
    « ordinateur » : traitement automatique de données
    \uncover<2->{
      $\Longrightarrow$ \red{environnement computationnel}
    }
  \end{center}

  \uncover<3->{\small{
      \begin{center}
        $\Big($
        \begin{tabular}{ccc}
          « ordinateur » $\approx$ instrument & et & « calcul » $\approx$ mesure
          \\
          environnement computationnel &$\leftrightarrow$& conditions expérimentales
        \end{tabular}
        $\Big)$
      \end{center}
   }}

  \uncover<4->{
    \begin{alertblock}{}
      \begin{center}
        Quel contrôle de l’{environnement computationnel} ?
      \end{center}
    \end{alertblock}
  }
\end{frame}



\begin{frame}[fragile]{Exemple d’un calcul}
  \begin{lstlisting}[language=C, caption={Fonction $J_0$ de Bessel en langage \texttt{C}}, captionpos=t]{Name=open-source}
    #include <stdio.h>
    #include <math.h>

    int main(){
      printf("%E\n", j0f(0x1.33d152p+1f));
    }
  \end{lstlisting}

  \begin{center}
    \begin{tabular}{lcl}
      Alice &voit :& 5.\blue{64}34\blue{4}0E-08 % #no ptions
      \\
      Carole &voit :& 5.\red{96}34\red{3}0E-08 % -lm -fno-builtin
    \end{tabular}
  \end{center}

  \begin{alertblock}{}
    \begin{center}
      Pourquoi ? Le code est disponible pourtant.
    \end{center}
  \end{alertblock}

  \small{%
    Établir si le différence est significative ou non est laissé à l'expertise
    des scientifiques du domaine.
  }
\end{frame}



\begin{frame}[fragile]{Quelques questions sur ce calcul}
  \begin{lstlisting}[basicstyle=\tiny, language=C]{Name=open-source}
    #include <stdio.h>
    #include <math.h>

    int main(){
      printf("%E\n", j0f(0x1.33d152p+1f));
    }
  \end{lstlisting}

  \begin{center}
    \begin{tabular}{lcl}
      Alice &voit :& 5.\blue{64}34\blue{4}0E-08 % #no options
      \\
      Carole &voit :& 5.\red{96}34\red{3}0E-08 % -lm -fno-builtin
    \end{tabular}
  \end{center}

  \begin{exampleblock}{}
    \begin{itemize}
    \item Quel compilateur ?
    \item Quelles bibliothèques (\texttt{<math.h>}) ?
    \item Quelles versions ?
    \item Quelles options de compilation ?
    \end{itemize}
  \end{exampleblock}
\end{frame}



\begin{frame}[fragile]{Par exemple: options de constructions (compilation)}
  \begin{center}
     \small{Alice et Carole utilisent « GCC à la version 11.2.0 »}
  \end{center}

  \begin{lstlisting}[basicstyle=\tiny, language=C]{Name=open-source}
    #include <stdio.h>
    #include <math.h>

    int main(){
      printf("%E\n", j0f(0x1.33d152p+1f));
    }
  \end{lstlisting}

  \vfill

  \begin{exampleblock}{}
    \begin{center}
      \begin{tabular}{rlll}
        \blue{\texttt{alice@laptop\$}} & \texttt{gcc bessel.c}
        && \texttt{\&\& ./a.out}
        \\
                                       & \texttt{5.643440E-08}
        \\
        \red{\texttt{carole@desktop\$}} & \texttt{gcc bessel.c}
        & \texttt{\mauve{-lm -fno-builtin}}
                                       & \texttt{\&\& ./a.out}
        \\
                                       & \texttt{5.963430E-08}
      \end{tabular}
    \end{center}
  \end{exampleblock}

  \begin{flushright}
    \small{(Ah, sacré \emph{constant folding})}
  \end{flushright}

  \uncover<2->{
  \begin{alertblock}{}
    \begin{center}
      Alice et Carole ont fait leur calcul dans
      \uline{\textbf{deux environnements computationnels différents}}
    \end{center}
  \end{alertblock}
  }\uncover<3->{
  \begin{center}
    \red{\textbf{Il faut donc plus que les numéros de versions}}
  \end{center}
  }
\end{frame}



\begin{frame}[fragile,label=p4]{Les questions d’un environnement computationnel}
  \begin{itemize}
  \item Quelles sont les sources des outils ?
  \item Quelles sont les outils requis pour la construction ?
  \item Quelles sont les outils requis pour l’exécution ?
  \item Comment chaque outil est-il produit ? (récursivement)
  \end{itemize}

  \begin{exampleblock}{}
    \begin{center}
      Répondre à ces questions signifie \textbf{contrôler la variabilité} de
      l’environnement computationnel
    \end{center}
  \end{exampleblock}

  \uncover<2->{
  \vfill

  \begin{alertblock}{}
    \begin{center}
      Comment capturer ces informations ?
    \end{center}
  \end{alertblock}

  \vfill
  \small{
    (Réponses usuelles : Gestionnaire de paquets (Conda, APT, Brew, \dots) ;
    \emph{Modulefiles} ; Conteneur ; etc.)
  }}
\end{frame}



\begin{frame}[fragile]{Enjeu de la reproductibilité en recherche}
  \begin{exampleblock}{D'un point de vue de la « méthode scientifique » :}
    \begin{center}
      {Tout l'enjeu est le \uline{\textbf{contrôle de la variabilité}}}
    \end{center}
  \end{exampleblock}

  \vfill

  \begin{exampleblock}{D'un point de vue de la construction du
      « savoir scientifique » (un caractère universel ?) :}
    \begin{itemize}
    \item Un observateur indépendant doit être capable d'observer le même
      résultat.
    \item L'observation doit être pérenne (dans une certaine mesure).
    \end{itemize}
  \end{exampleblock}

  \uncover<2->{
  \vfill

  \begin{alertblock}{}
    Dans un monde où (presque) tout est donnée numérique (\emph{data}),
    \begin{center}
      \textbf{comment refaire plus tard et là-bas ce qui a été fait aujourd’hui et ici ?}
    \end{center}
    \begin{flushright}
      (sous entendu avec un « ordinateur »)
    \end{flushright}
  \end{alertblock}
  }
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{frame}<presentation>[label=solutions, fragile]{Solution(s)}
  \begin{enumerate}
  \item gestionnaire de paquets : APT (Debian/Ubuntu), YUM (RedHat), etc.
  \item gestionnaire d'environnements : Modulefiles, Conda, etc.
  \item conteneur : Docker, Singularity
  \end{enumerate}

  \begin{alertblock}{}
    \begin{center}
      Guix = \#1 + \#2 + \#3
    \end{center}
  \end{alertblock}

  \begin{description}
  \item[APT, Yum] Difficile de faire coexister plusieurs versions ou
    revenir en arrière ?
  \item[Modulefiles] Comment sont-ils maintenus ? (qui les utilise sur son
    \emph{laptop} ?)
  \item[Conda] Quelle granularité sur la transparence ? (qui sait comment a
    été produit PyTorch dans \texttt{conda install torch} ?
    \href{http://hpc.guix.info/blog/2021/09/whats-in-a-package/}%
    {\scriptsize{(lien)}})
  \item[Docker] Dockerfile basé sur APT, YUM etc.
    \begin{minipage}{1.0\linewidth}
      \begin{exampleblock}{}
\begin{verbatim}
RUN apt-get update && apt-get install
\end{verbatim}
      \end{exampleblock}
    \end{minipage}
  \end{description}
\end{frame}



\begin{frame}<presentation>[label=steroide]{Guix est gestionnaire
    d’environnements sous \emph{stéroïde}}
  \begin{tabular}{lr}
    un \uline{\textbf{gestionnaire de paquets}}
    & (comme APT, Yum, etc.)
    \\
    \ \ transactionnel et déclaratif
    & (revenir en arrière, versions concurrentes)
    \\
    \ \ \ \ qui produit des \uline{\textbf{\emph{packs} distribuables}}
    & (conteneur Docker ou Singularity)
    \\
    \ \ \  \ \ \ qui génèrent des \textbf{\uline{\emph{machines virtuelles}} isolées}
    & (\emph{à la} Ansible ou Packer)
    \\
    \ \ \ \ \ \ \ \ sur lequel on construit une distribution Linux
    & (mieux que les \smiley{autres ?})
    \\
    \ \ \ \ \ \ \ \ \ \ \dots et aussi une bibliothèque Scheme\dots
    & (extensibilité, que voilà !)
  \end{tabular}

  \vfill

  \begin{alertblock}{}
    Ce qu’il faut retenir :
    \begin{center}
      la gestion de paquets \emph{fonctionnelle}
      \quad \quad
      $\Rightarrow$ reproductibilité
    \end{center}
  \end{alertblock}

  \begin{center}
    \textbf{\alert{Guix fonctionne sur n'importe quelle distribution Linux}}
  \end{center}

  \vfill

  \begin{flushright}
    \small{%
      (Facile à essayer\dots)
    }
  \end{flushright}
\end{frame}



\begin{frame}<presentation>[fragile]{Sur une \emph{distro externe}}
  \begin{alertblock}{}
    \begin{center}
      Guix s’installe sur \textbf{\uline{n’importe quelle distribution}} Linux récente.
    \end{center}
  \end{alertblock}
  Il faut les droits administrateur (\texttt{root}) pour l’installation.

  \begin{exampleblock}{}
\begin{verbatim}
$ cd /tmp
$ wget https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh
$ chmod +x guix-install.sh
$ sudo ./guix-install.sh
\end{verbatim}
  \end{exampleblock}

  \begin{center}
    (Quelques réglages supplémentaires, voir le manuel)
  \end{center}

  Pour commencer :
  \begin{center}
    \begin{minipage}{0.75\linewidth}
\begin{verbatim}
$ guix help
\end{verbatim}
    \end{minipage}
  \end{center}
\end{frame}



\begin{frame}<presentation>[label=cli-basic, fragile]{Guix, un gestionnaire de paquets comme les autres !}
  \begin{exampleblock}{}
\begin{verbatim}
guix search dynamically-typed programming language # 1.
guix show    python                                # 2.
guix install python                                # 3.
guix install python-ipython python-numpy           # 4.
guix remove  python-ipython                        # 5.
guix install python-matplotlib python-scipy        # 6.
\end{verbatim}
  \end{exampleblock}

  \begin{center}
    alias de \texttt{guix package}, p. ex. \verb+guix package --install+
  \end{center}

  \begin{alertblock}{Transactionnel}
\begin{verbatim}
guix package --install python                                    # 3.
guix package --install python-ipython python-numpy               # 4.
guix package -r python-ipython -i python-matplotlib python-scipy # 5. & 6.
\end{verbatim}
  \end{alertblock}
\end{frame}



\begin{frame}<presentation>[label=pkg-man]{Guix, un gestionnaire de paquets comme les autres ?}
  \begin{alertblock}{}
    \begin{itemize}
    \item Interface \emph{ligne de commande} comme les autres gestionnaires de
      paquets
    \item Installation/suppression sans privilège particulier
    \item Transactionnel (= pas d'état «~\emph{cassé}~»)
    \item \emph{Substituts} binaires (téléchargement d'éléments pré-construits)
    \end{itemize}
  \end{alertblock}
  \begin{center}
    \textbf{15 min, c’est \smiley{court} :}
  \end{center}

  \begin{exampleblock}{}
    \begin{itemize}
    \item \sout{Les \emph{profils} et leur composition}
    \item \sout{Gestion déclarative}
    \item \sout{Environnement isolé à la volée}
    \item \sout{Génération image Docker}
    \end{itemize}
  \end{exampleblock}

  \begin{center}
    mini-tuto 1h aux JRES -- Marseille, 2022
    \href{https://zimoun.gitlab.io/jres22-tuto-guix/}{(lien PDF)}
    \href{https://replay.jres.org/w/3TuYmocHwKtzs7q1VtL1GB}{(lien Vidéo)}

    \href{https://replay.jres.org/w/3TuYmocHwKtzs7q1VtL1GB}%
    {https://replay.jres.org/w/3TuYmocHwKtzs7q1VtL1GB}
  \end{center}
\end{frame}



\begin{frame}<presentation>[plain,fragile,noframenumbering]{}
  Très bien tout cela, mais en quoi est-ce \uline{reproductible} ?

  \vfill{}
  \begin{center}
    \textbf{Parlons de versions !}
  \end{center}
  \vfill{}
  \begin{flushright}
    (Exemple : Carole collabore avec Alice)
  \end{flushright}
\end{frame}



\begin{frame}[fragile]{Alice dit « GCC à la version 11.2.0 »}
  \vspace{-0.1cm}
  \includegraphics[width=\textwidth]{static/graph.png}
  \vfill{}
  \begin{alertblock}{}
    \begin{center}
      Est-ce la même version de GCC si \texttt{mpfr} est à la version 4.0 ?
    \end{center}
  \end{alertblock}
  \vfill
  \begin{flushright}
    Graphe complet : 43 ou 104 ou 125 ou 218 n\oe uds

    \small
    (suivant ce que l’on considère comme graine binaire du \emph{bootstrap})
  \end{flushright}
\end{frame}



\begin{frame}<presentation>[label=version, fragile]{Quelle est ma version de Guix ?}
  \begin{exampleblock}{}
\begin{verbatim}
$ guix describe
Generation 76	Apr 25 2022 12:44:37	(current)
  guix eb34ff1
    repository URL: https://git.savannah.gnu.org/git/guix.git
    branch: master
    commit: eb34ff16cc9038880e87e1a58a93331fca37ad92

$ guix --version
guix (GNU Guix) eb34ff16cc9038880e87e1a58a93331fca37ad92
\end{verbatim}
  \end{exampleblock}

  % \vspace{-0.175cm}
  \begin{alertblock}{}
    \begin{center}
      Un état fixe toute la collection des paquets et de Guix lui-même
    \end{center}
  \end{alertblock}

  (Un état peut contenir plusieurs canaux (\emph{channel} = dépôt Git),\\
  \hfill avec des URL, branches ou commits divers et variés)
\end{frame}



\begin{frame}<presentation>[label=dag, fragile]{État = Graphe Acyclique Dirigé (\emph{DAG})}
  \vspace{-0.5cm}
  \begin{center}
    \texttt{guix graph -{}-max-depth=6 python | dot -Tpng > graph-python.png}
  \end{center}

  \vfill{}
  \includegraphics[width=\textwidth]{static/graph-python.png}
  \vfill{}

  Graphe complet : Python = 137 n\oe uds, Numpy = 189, Matplotlib = 915, Scipy
  = 1439 n\oe uds
  % graph python-scipy -t bag | grep label | cut -f2 -d'[' | sort | uniq | wc -l
\end{frame}


\begin{frame}[fragile]{Révision = un graphe spécifique}
  \begin{alertblock}{}
    \begin{center}
      « GCC à la version 11.2.0 » = un graphe
    \end{center}
  \end{alertblock}

\begin{verbatim}
$ guix describe
Generation 76	Apr 25 2022 12:44:37	(current)
  guix eb34ff1
    repository URL: https://git.savannah.gnu.org/git/guix.git
    branch: master
    commit: eb34ff16cc9038880e87e1a58a93331fca37ad92
\end{verbatim}

  \begin{alertblock}{}
    \begin{center}
      La révision \texttt{eb34ff1} capture \textbf{tout} le graphe
    \end{center}
  \end{alertblock}

  \begin{exampleblock}{}
    \begin{itemize}
    \item Alice dit « j’ai utilisé Guix à la révision \texttt{eb34ff1} »
    \item Carole connaît toutes les informations pour reproduire le même environnement
    \end{itemize}
  \end{exampleblock}
\end{frame}



\begin{frame}[fragile]{Collaboration%
    \uncover<2->{\hfill \hspace{4cm} \small{gestionnaire de paquets $\leftrightarrow$ état}}
    \uncover<4->{\small{$\leftrightarrow$ gestionnaire de graphe}}}

  \uncover<3->{
  \begin{alertblock}{}
    \begin{center}
      collaborer = partager un environnement computationnel
      \uncover<4->{$\Rightarrow$ partager un graphe}
    \end{center}
  \end{alertblock}
  }

  \begin{description}
  \item[Alice] décrit son environnement :
    \begin{itemize}
    \item<1-> la liste des outils avec le fichier \uline{\texttt{liste-outils.scm}}
    \item<2-> la révision (Guix lui-même et aussi potentiellement les autres canaux):
      \begin{center}
        \texttt{guix describe -f channels > \uline{état-alice.scm}}
      \end{center}
    \end{itemize}
    \uncover<1->{
    génère son environnement avec, e.g.,
    \begin{center}
      \texttt{guix shell -m \uline{liste-outils.scm}}
    \end{center}
    }\uncover<5->{
    et {\textbf{partage ses deux fichiers}} :
    \uline{\texttt{état-alice.scm}} et \uline{\texttt{liste-outils.scm}}.
    }
  \item<6->[Carole] génère le même environnement {\textbf{à partir des \uline{deux fichiers}}} d’Alice,
    \begin{center}
      \texttt{guix time-machine -C \uline{état-alice.scm} -{}- shell -m \uline{liste-outils.scm}}
    \end{center}
  \item<7->[Dan] peut donc aussi avoir le même environnement qu’Alice et Carole.
  \end{description}
\end{frame}



\begin{frame}<presentation>[label=time-machine, fragile]{Reproductibilité en arrière, en avant :
    \texttt{guix time-machine}}

  \vspace{-1cm}
  \begin{center}
    \begin{tikzpicture}
      % draw horizontal line
      \draw[thick, -Triangle] (0,0) -- (12,0) node[font=\scriptsize,below left=3pt and -8pt]{time};

      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] at (0,-.3) {2018};

      % draw vertical lines
      \foreach \x in {1,...,11}
      \draw (\x cm,3pt) -- (\x cm,-3pt);

      \foreach \x/\descr in {3/Carole,6/Alice,8/Bob}
      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] at (\x,-.3) {$\descr$};

      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] (Carole) at (3,+.3)  {d7e57e};
      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] (Alice) at (6,+.3)  {eb34ff1};
      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] (Dan) at (8,+.3)  {3682bd};

      \draw[->] (Carole.north) to [out=50,in=150] (Alice.west);
      \draw[->] (Dan.north) to [out=100,in=50] (Alice.east);

      \draw[lightgray!0!red, line width=4pt] (2,-.6) -- +(8,0);
      \foreach \x/\percol in {1/75,9/75}
      \draw[lightgray!\percol!red, line width=4pt] (\x,-.6) -- +(1,0);

      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] at (11,-.3)  {$Dan$};
      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] (Dan) at (11,+.3)  {c99c3d};
      \draw[dashed,->] (Dan.north) to [out=40,in=90] (Alice.north);

    \end{tikzpicture}
  \end{center}

  \begin{exampleblock}{}
    Pour être \uline{reproductible dans le temps}, il faut :
    \begin{itemize}
    \item Une préservation de \textbf{tous} les codes source~%
      \small{%
        (\href{https://ngyro.com/pog-reports/latest/}%
        {$\approx 75\%$ archivés \tiny{(lien)}\small} dans~%
        \href{https://www.softwareheritage.org/}%
        {Software Heritage \tiny{(lien)}\small})%
      }\normalsize
    \item Une \emph{backward} compatibilité du noyau Linux
    \item Une compatibilité du \emph{hardware} (p. ex. CPU, disque dur \tiny{(NVMe)}\normalsize, etc.)
    \end{itemize}
  \end{exampleblock}

  \begin{alertblock}{}
    \begin{center}
      Quelle est la taille de la fenêtre temporelle avec les 3 conditions
      satisfaites ?
    \end{center}
  \end{alertblock}
  \begin{center}
    \scriptsize{(À ma connaissance, le projet Guix réalise une expérimentation
      grandeur nature et quasi-unique depuis sa v1.0 en 2019)}
  \end{center}
\end{frame}



\begin{frame}<presentation>[plain,fragile,noframenumbering]{}
  \begin{center}
    \textbf{Au final}
  \end{center}
\end{frame}



\begin{frame}[fragile]{comment refaire plus tard et là-bas ce qui a été fait aujourd’hui et ici ?}
  \begin{alertblock}{}
    \begin{center}
      \textbf{traçabilité et transparence}
    \end{center}
  \end{alertblock}
  \begin{center}
    \emph{être capable d’étudier bogue-à-bogue}
  \end{center}

  \begin{exampleblock}{}
    Guix devrait s’occuper de tout
    \begin{center}
      \texttt{guix time-machine -C channels.scm -{}- \emph{cmd} -m manifest.scm}
    \end{center}
    si on spécifie
    \begin{description}
    \item[« comment construire »] \hfill \texttt{channels.scm}
    \item[« quoi construire »] \hfill \texttt{manifest.scm}
    \end{description}
  \end{exampleblock}
\end{frame}



\begin{frame}<presentation>[label=continuum2, fragile]{\dots Guix est un \emph{continuum}%
    \hfill \hspace{4.2cm} \small{(sur n'importe quelle distribution Linux)}}

  \vspace{-0.5cm}

  \begin{center}
    \begin{tabular}{llr}
      un \uline{\textbf{gestionnaire de paquets}} déclaratif
      & \texttt{guix package}
      & (\texttt{-m} \emph{manifest})
      \\
      \ \ temporairement étendu à la volée
      & \texttt{guix shell}
      & (\texttt{-{}-container})
      \\
      \ \ \ \ maîtrisant exactement l'\emph{état}
      & \texttt{guix time-machine}
      & (\texttt{-C} \emph{channels})
      \\
      \ \ \ \ \ \ qui produit des \uline{\textbf{\emph{packs} distribuables}}
      & \texttt{guix pack}
      & (\texttt{-f docker})
      \\
      \ \ \ \ \ \ \ \ qui génèrent des \textbf{\uline{\emph{machines virtuelles}} isolées}
      & \texttt{guix sytem vm}
      \\
      \ \ \ \ \ \ \ \ \ \ \ \ \ et aussi une bibliothèque Scheme
      & \texttt{guix repl}
      & (\emph{extensions})
      \\
      $\Big($
      la distribution Linux elle-même
      & \texttt{config.scm}
      & (Guix System)
        $\Big)$
    \end{tabular}

    \begin{alertblock}{}
      \begin{center}
        \textbf{%
          Guix permet un contrôle fin du graphe de configuration sous-jacent
        }
      \end{center}
    \end{alertblock}
  \end{center}

  \begin{exampleblock}{}
    \begin{center}
      \texttt{guix time-machine -C \uline{état.scm} -{}-}
      \emph{commande options}
      \uline{\texttt{une-config.scm}}
    \end{center}
  \end{exampleblock}

  \begin{alertblock}{}
    \begin{center}
      \uline{\texttt{une-config.scm}} est \textbf{reproductible}
      d'une machine à l'autre et dans le temps
    \end{center}
  \end{alertblock}
\end{frame}



\begin{frame}<presentation>[label=prod]{En production}
  \vspace{-0.2cm}
  \begin{exampleblock}{}
    \begin{tabular}{lcrl|c}
      \textbf{Grid’5000}           &          &  828-nodes  &(12,000+ cores, 31 clusters) &(France)     \\
      \textbf{GliCID (CCIPL)}      & Nantes   &  392-nodes  &(7500+ cores)                 &(France)    \\
      \textbf{PlaFrIM Inria}       & Bordeaux &  120-nodes  &(3000+ cores)                 &(France)    \\
      \textbf{GriCAD}              & Grenoble &  72-nodes   &(1000+ cores)                 &(France)    \\
      \textbf{Max Delbrück Center} & Berlin   &  250-nodes  &+ workstations                &(Allemagne) \\
      \textbf{UMC}                 & Utrecht  &  68-nodes   &(1000+ cores)                 &(Pays-Bas)  \\
      \textbf{UTHSC Pangenome}     &          &  11-nodes   &(264 cores)                   &(USA)       \\
      \\
      \quad (le vôtre ?)
    \end{tabular}
  \end{exampleblock}

  \vspace{-0.65cm}
  \begin{center}
    \includegraphics[width=0.25\paperwidth]{static/guixhpc-logo-transparent-white}

    \vspace{-0.55cm}

    \href{https://hpc.guix.info/}{\texttt{https://hpc.guix.info}}
  \end{center}

  \begin{minipage}{0.9\linewidth}
  \href{https://doi.org/10.1038/s41597-022-01720-9}%
  {Toward practical transparent verifiable and long-term reproducible research}

  \hspace{0.675\linewidth}
  \href{https://doi.org/10.1038/s41597-022-01720-9}%
  {using Guix \small{(lien)}}
  \end{minipage}

  \vspace{-1.cm}
  \begin{flushright}
    \includegraphics[width=0.175\paperwidth]{static/cafe-guix}
  \end{flushright}
\end{frame}



\begin{frame}[plain, noframenumbering]{}
  \begin{center}
    \vfill{}
    \Large{%
      \textbf{%
        Des questions ?%
      }}\normalsize

    \vfill{}
    \texttt{guix-science@gnu.org}

    \vfill{}
    \includegraphics[width=0.2\paperwidth]{static/cafe-guix}\\
    \href{https://hpc.guix.info/events/2022/café-guix/}{\texttt{https://hpc.guix.info/events/2022/café-guix/}}

    \vfill{}
    \vspace{1.5cm}
    Cette présentation est archivée.\\
    \href {https://archive.softwareheritage.org/swh:1:dir:80ed9280e64392434b9c3dd8d4577161295c87cc;origin=https://gitlab.com/zimoun/bluehats-2022;visit=swh:1:snp:aa969caea59f6e2719cbee7d3f19846b4f32b4cb;anchor=swh:1:rev:97590a32d5e167816d90e1463a17fba882d4791e}%
    {(Software Heritage id swh:1:dir:80ed9280e64392434b9c3dd8d4577161295c87cc)}
  \end{center}
\end{frame}

\appendix

\begin{frame}<presentation>[label=declarative, fragile, noframenumbering]{Gestion déclarative%
    \hfill\small{(%
      \texttt{guix shell python python-numpy -{}-export-manifest})}}
  \vspace{-0.3cm}
  \begin{center}
    déclaratif = fichier de configuration
  \end{center}

  \begin{exampleblock}{%
      Un fichier \texttt{some-python.scm} peut contenir cette déclaration :%
    }
    \lstinputlisting[language=Scheme,columns=space-flexible]{example/some-python.scm}
  \end{exampleblock}

  \begin{center}
    \verb+guix package --manifest=some-python.scm+
  \end{center}
  équivalent à
  \begin{center}
    \verb+guix install python python-numpy+
  \end{center}
\end{frame}


\begin{frame}<presentation>[label=declarative2, fragile, noframenumbering]{Gestion \emph{déclarative} : remarques}
  \begin{description}
  \item[Version ?] Nous le verrons dans la suite
    \medskip
  \item[Langage ?] \emph{Domain-Specific Language} (DSL) basé sur
    \href{https://fr.wikipedia.org/wiki/Scheme}%
    {Scheme}
    \href{https://fr.wikipedia.org/wiki/Lisp}%
    {(«~langage fonctionnel Lisp~»)}%
    \medskip
    \begin{itemize}
    \item \verb+(Oui (quand (= Lisp parenthèses) (baroque)))+
      \medskip
    \item Mais \uline{\textbf{continuum}} :
      \begin{enumerate}
      \item configuration (\texttt{manifest})
      \item définition des paquets (ou services)
      \item extension
      \item le c\oe ur est écrit aussi en Scheme
      \end{enumerate}
    \end{itemize}
  \end{description}
  \begin{alertblock}{}
    \begin{center}
      Guix est \textbf{adaptable} à ses besoins
    \end{center}
  \end{alertblock}

  \vfill

  \footnotesize{%
    \href{https://fr.wikipedia.org/wiki/Programmation_déclarative}{Déclaratif}
    vs
    \href{https://fr.wikipedia.org/wiki/Programmation_impérative}{Impératif}
  } \hfill \scriptsize{%
    (%
    et non pas
    % \hfill
    Donnée inerte vs Programme%
    )
  }

  \footnotesize{%
    Programmation déclarative = programmation fonctionnelle ou descriptive (\LaTeX) ou logique (Prolog)
  }
\end{frame}



\begin{frame}<presentation>[label=trans-declarative, fragile, noframenumbering]{Gestion déclarative : exemple de transformation
    \small{%
    (\href{https://fr.wikipedia.org/wiki/Machine_de_Rube_Goldberg}%
    {machine de \smiley{Goldberg} \tiny{(lien)}})}} % \ddot\smile
  \vspace{-0.3cm}
  \begin{exampleblock}{}
    \lstinputlisting[language=Scheme,columns=space-flexible]{example/some-python-bis.scm}
  \end{exampleblock}
  Guix DSL, \somevariable{\emph{variables}}, \someguile{Scheme} et \somestring{chaîne de caractères}.
\end{frame}


\begin{frame}<presentation>[label=channel, fragile, noframenumbering]{Exemple \texttt{état.scm}%
    \hfill \small{(\texttt{guix describe -f channels-sans-intro})}}
  \begin{exampleblock}{}
    \lstinputlisting[language=Scheme,columns=space-flexible]{example/état.scm}
  \end{exampleblock}
\end{frame}


\end{document}
